# tutorial: https://www.learnpython.org/en/Loops

# define lists
my_list_1 = [5, 2, 5, 7, 9]
my_list_2 = ["Ion", "Ana", "Gigel", "Vasile"]
my_list_3 = [4, "B", 55, "XYZ", 44.3]
my_tuple = (4, "B", 55, "XYZ", 44.3)

# slicing a string
my_str = "Ana are mere"
print(my_str[:3])
print(my_str[:-5])

# slicing a list
print(my_list_2[0:-1])
print(my_list_2[-2])

# for loop

suma = 0
for x in my_list_1:
    suma = suma + x
print('suma calculata cu for este: ', suma)

print('suma calculata cu functia sum() este:', sum(my_list_1))


# inversul unei lite
for x in reversed(range(1, 11)):
    print(x)

# cast din list in tuple si invers
print(list(my_tuple))
print(tuple(my_list_1))

# range function
for x in range(20):
    print(x)


# while
x = 10
while x < 10:
    print(x)
    x += 1

# exercitiu: implementati o bucla "while True:" care se va opri cand x ajunge la 15
