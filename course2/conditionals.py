# tutorial: https://www.learnpython.org/en/Conditions

a = 5
x = input('b=')

if not x or not x.isdigit():
    print('ai dat buleala')
    exit()

b = int(x)

if b > a:
    print(f"Success! Numarul introdus este mai mare decat {a}")
elif b == a:
    print("Egale!")
else:
    print(f"Fail! Numarul introdus NU este mai mare decat {a}")

# operatori: in   ==     >     <     >=    <=    not

# introduceti o propozitie de la tastatura, si verificati daca contine "." (punct)
if "." in input('Introdu o propozitie:'):
    print("am gasit punct!")

# if len ...

# exercitiu: avand functia input() cu urmatoarea intrebare: "Ce extensie au
# fisierele python? ",
# sa se creeze o conditie care verifica daca raspunsul de la input este corect
