# Getting started with python (for QAs)

This repository contains the sample code used to learn python from scratch.
Many exercises were inpsired from [https://www.learnpython.org/](learnpython.org).

If you're totally new to python, following courses from learnpython.org is a perfect way to get started.
As you go trought those tutorials, you may also have a look in this repository, at our sample code.

##  Installing Python
Download python from [https://www.python.org/downloads/](https://www.python.org/downloads/) (version 3). When installing, make sure you check "Add python to environment variables...".
 
## Install PyCharm (code editor / IDE)
Download PyCharm Community Edition [https://www.jetbrains.com/pycharm/download](https://www.jetbrains.com/pycharm/download)
It will automatically detect python interpreter (previously installed).

## Run hello world
Create a new project/folder somewhere in your system (`C:\Projects\python_course` for example), and open it in PyCharm.

Create a new file (ex: `hello.py`, write `print("Hello world")` in it, right click and "Run". The output should be printed in console. 

You may now proceed to the tutorials from [https://www.learnpython.org/](learnpython.org).

## Homeworks
In each session, we had homeworks to do. They are added in Trello: [https://trello.com/b/r3m8RvtD/qa-buc](https://trello.com/b/r3m8RvtD/qa-buc)
Ask colleagus to add you in the board.

You will need to solve those homeworks too. 

At any point, if you don't understand something, you can write me in private on rocket chat.


have fun! :D 