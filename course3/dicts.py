# dictionary declaration
my_dict = {
    'some_key': 'some_value',
    5: 'some_val for 5',
    True: 'asdas',
    'asdasaasd': True,
}

print(my_dict)
# # access values, by keys
# print(my_dict['some_key'])
#
# # change values by key
# my_dict['some_key'] = 'altceva'
# print(my_dict)


# assign new keys/values
my_dict['some_key'] = 'changed_value'
my_dict['some_other_key'] = 'some_other_value'


contacts = {
    'Gigel': '0745334332',
    'Ionel': '0745335531',
    'Brusturel': '0755452875',
}

contacts2 = contacts.copy()

for key in contacts2:
    contacts2[key] = '+4' + contacts2[key]

for key in contacts:
    print('nume', key, 'telefon', contacts[key])


# a simplier way to unpack key/value

for key, value in contacts.items():
    print('nume', key, 'telefon', value)

contacts['Felix'] = '08773373737'

contacts_from_sim_card = {
    'Tata': '0766363636',
    'Mama': '0766363637',
}
contacts.update(contacts_from_sim_card)

print(contacts)

del contacts['Felix']
print(contacts)

# check if key exists in dict
nume = input('nume=')

if nume in contacts:
    print(f'{nume} exista si are tel: {contacts[nume]}')


# get contact, if exists, without error
telefon_gigel = contacts.get('Gigel', None)
telefon_ionel = contacts.get('Ionel', None)
print(telefon_gigel)
print(telefon_ionel)

print(contacts.get(input('nume='), ' nu s-a gasit '))

# exercitiu: pe baza unui NUME luat de la tastatura
