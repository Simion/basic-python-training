
__all__ = ['welcome', 'welcome2', 'create_contact']


def welcome():
    print('Welcome bre!')


# welcome()


def welcome2(username, email=None, phone=None):
    print(f'Welcome {username} / {email} / {phone}!')


# welcome2('gigel', phone='023423423')
# welcome2(email='ceva@ceve.asd', username='gigel')


def sum_of_numbers(a, b, *args):
    print(a)
    print(b)
    print(args)


numere = [4, 5, 7, 2, 5, 7]

# sum_of_numbers(1, 2, *numere, 3)

# sum_of_numbers(1, 2, 6, 5, 7, 3)


xx = 231


def create_contact(name, email, phone=None, **kwargs):
    global xx

    print(f'Added contact with info: {name} / {email} / {phone}')
    print(f'Extra fields: {kwargs}')


extra = {
    'gender': 'F',
    'age': 24,
    'height': 180,
    'weight': 88,
    'eyes': 'brown',
    'hair_color': 'dark',
}
# create_contact('Gigel', 'gigi@gmail.com', '07553', **extra)
# create_contact('Gigel', 'gigi@gmail.com', '07553', gender='F', age=24, height=180, weight=88, eyes='brown', hair_color='dark')
